if ActiveRecord::Base.connection.table_exists?(:settings)
  Rails.configuration.to_prepare do

    #Patches
    require_dependency 'jumay_biz/patches/queries_helper_patch'

    #Helpers
    require_dependency 'jumay_biz/helpers'

    #Helpers
    require_dependency 'jumay_biz/hooks'

    module JumayBiz
      def self.settings
        Setting[:plugin_jumay_biz].blank? ? {} : Setting[:plugin_jumay_biz]
      end
    end
  end
end
