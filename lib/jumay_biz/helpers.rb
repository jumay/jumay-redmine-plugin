require_dependency 'queries_helper'

module JumayBiz
  module Helpers
    def jumay_query_filters_options_json(query)
      logger.info("JumayBiz::Helpers::jumay_query_filters_options_json => " + JumayBiz.settings[:query_filters].to_s)
      logger.info("*********************")
      counter = 0
      json = {}

      options = filters_options_for_select_without_jumay_client_mode(query)
      arrays = jumay_options_for_select_to_array(options)

      if JumayBiz.settings[:query_filters].nil? || JumayBiz.settings[:query_filters].to_s.strip.empty?
        allowed_filters = []
      else
        allowed_filters = JumayBiz.settings[:query_filters].split(",")
      end
      #logger.info(allowed_filters)
      grouped = arrays[0]
      ungrouped = arrays[1]

      ungrouped.each{|o|
        counter = counter + 1
        opt = { :id => o[0], :text => o[1] }
        allowed_filters.each{|f|
          if(f.eql? o[0])
            opt.merge!(selected: true)
          end
        }
        json.store(counter, opt)
      }

      grouped.each{|g|
        g[1].each{|o|
          counter = counter + 1
          opt = { :id => o[0], :text => o[1] }
          allowed_filters.each{|f|
            if(f.eql? o[0])
              opt.merge!(selected: true)
            end
          }
          json.store(counter, opt)
        }
      }

      json.to_json
    end

    def jumay_query_available_inline_columns(query)
      logger.info("JumayBiz::Helpers::jumay_query_available_inline_columns => " + JumayBiz.settings[:query_columns].to_s)
      logger.info("*********************")
      columns = (query.available_inline_columns).reject(&:frozen?).collect {|column| [column.caption, column.name]}
      #logger.info(columns)

      if JumayBiz.settings[:query_columns].nil? || JumayBiz.settings[:query_columns].to_s.strip.empty?
        allowed_columns = []
      else
        allowed_columns = JumayBiz.settings[:query_columns].split(",")
      end

      json = {}
      counter = 0

      columns.each{|c|
        counter = counter + 1
        opt = { :id => c[1], :text => c[0] }
        allowed_columns.each{|a|
          #logger.info(c[1].to_s + " & " + a)
          if(a.eql? (c[1].to_s))
            opt.merge!(selected: true)
          end
        }
        json.store(counter, opt)
      }

      json.to_json
    end

    def jumay_queries_columns_hidden_field_tags
      logger.info("JumayBiz::Helpers::jumay_queries_columns_hidden_field_tags")
      logger.info("*********************")
      if JumayBiz.settings[:query_columns].nil? || JumayBiz.settings[:query_columns].to_s.strip.empty?
        allowed_columns = []
      else
        allowed_columns = JumayBiz.settings[:query_columns].split(",")
      end

      hidden_tags = hidden_field_tag('c[]', '', :id => nil)
      allowed_columns.each{|c|
        hidden_tags << hidden_field_tag('c[]', c, :id => nil)
      }

      hidden_tags
    end

    def jumay_get_proyect_manager(users_by_role)
      logger.info("JumayBiz::Helpers::jumay_get_proyect_manager => " + JumayBiz.settings[:default_role_assignee_new_issue].to_s)
      logger.info("*********************")
      proyect_manager = 0
      if users_by_role.any?
        users_by_role.keys.sort.each{|role|
          #logger.info("Role: " + role.to_s)
          if (role.to_s.strip.eql? JumayBiz.settings[:default_role_assignee_new_issue].to_s.strip)
            #logger.info("*** MATCH ***")
            users_by_role[role].sort.collect{|u|
              #logger.info("\tUser: " + u.to_s)
              if u.id != 1
                proyect_manager = u.id
              end
            }
          end
        }
      end

      if proyect_manager == 0
        proyect_manager = 1
      end

      proyect_manager
    end

    def jumay_trackers_options_for_select(issue)
      #Copy from helpers/issues_helper
      logger.info("JumayBiz::Helpers::jumay_trackers_options_for_select")
      logger.info("*********************")
      trackers = issue.allowed_target_trackers
      if issue.new_record? && issue.parent_issue_id.present?
        trackers = trackers.reject do |tracker|
          issue.tracker_id != tracker.id && tracker.disabled_core_fields.include?('parent_issue_id')
        end
      end
      trackers.collect {|t| [t.name, t.id]}
    end

    def jumay_options_for_select_to_array(options)
      grouped = []
      ungrouped = []

      grouped_str = options.scan(/(<optgroup\slabel="[\w\s]+">)\s?((<option\svalue="[\w\s]*">[a-zA-Z0-9\sáéíóúñÁÉÍÓÚÑ]+<\/option>\s?)+)<\/optgroup>/)
      ungrouped_str = options.gsub(/(<optgroup\slabel="[\w\s]+">)\s?((<option\svalue="[\w\s]*">[a-zA-Z0-9\sáéíóúñÁÉÍÓÚÑ]+<\/option>\s?)+)<\/optgroup>/, "")

      grouped_str.each{|m|
        gr = []
        gr << m[0].gsub(/<optgroup label="/, "").gsub(/">/, "") #Labellogger.info("********************** PATCH JUMAY WORKING **********************")
        gr << m[1].scan(/<option\svalue="([\w\s]*)">([a-zA-Z0-9\sáéíóúñÁÉÍÓÚÑ]+)<\/option>/) #opciones: value, text
        grouped << gr
      }
      ungrouped = ungrouped_str.scan(/<option\svalue="([\w\s]*)">([a-zA-Z0-9\sáéíóúñÁÉÍÓÚÑ]+)<\/option>/)

      #logger.info(grouped)
      #logger.info(ungrouped)

      if !User.current.admin && User.current.allowed_to?(:only_show_own_issues, @project)
        ungrouped_raw = []
        ungrouped.each{|o|
          if !(o[0].eql? 'author_id')
            ungrouped_raw << o
          end
        }
        ungrouped = ungrouped_raw
      end

      arrays = [grouped, ungrouped]
      return arrays
    end
  end
end

ActionView::Base.send :include, JumayBiz::Helpers
