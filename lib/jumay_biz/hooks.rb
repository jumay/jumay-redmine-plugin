#By &mT

module JumayBiz
  class JumayBizHookListener < Redmine::Hook::ViewListener
    render_on(:view_layouts_base_html_head, partial: 'global_html_header')
  end
end
