#http://www.redmine.org/projects/redmine/wiki/Plugin_Internals#Wrapping-an-existing-method
require_dependency 'queries_helper'

module JumayBiz
  module Patches
    module QueriesHelperPatch
      def self.included(base) # :nodoc:
        base.send(:include, InstanceMethods)

        base.class_eval do
          unloadable # Send unloadable so it will not be unloaded in development

          alias_method_chain :filters_options_for_select, :jumay_client_mode
        end
      end

      module InstanceMethods
        def filters_options_for_select_with_jumay_client_mode(query)
          logger.info("JumayBiz::Patches::QueriesHelperPatch::filters_options_for_select_with_jumay_client_mode")
          options = filters_options_for_select_without_jumay_client_mode(query)

          arrays = jumay_options_for_select_to_array(options)

          grouped = arrays[0]
          ungrouped = arrays[1]

          if !User.current.admin && User.current.allowed_to?(:enable_client_mode, @project)

            if JumayBiz.settings[:query_filters].nil? || JumayBiz.settings[:query_filters].to_s.strip.empty?
              allowed_filters = []
            else
              allowed_filters = JumayBiz.settings[:query_filters].split(",")
            end

            grouped_allowed = []
            ungrouped_allowed = []

            grouped.each{|g|
              group = [g[0]]
              group_added = false
              group_opcs = []
              g[1].each{|o|
                allowed_filters.each{|f|
                  if(f.eql? o[0])
                    group_opcs << o.reverse
                    group_added = true
                  end
                }
              }
              if(group_added)
                group << group_opcs
                grouped_allowed << group
              end
            }

            ungrouped.each{|o|
              allowed_filters.each{|f|
                if(f.eql? o[0])
                  ungrouped_allowed << o.reverse
                end
              }
            }

            s = options_for_select([[]] + ungrouped_allowed)
            s << grouped_options_for_select(grouped_allowed)

            options = s
          elsif !User.current.admin && User.current.allowed_to?(:only_show_own_issues, @project)
            grouped_updated = []
            ungrouped_updated = []

            grouped.each{|g|
              group = [g[0]]
              group_opcs = []
              g[1].each{|o|
                group_opcs << o.reverse
              }
              group << group_opcs
              grouped_allowed << group
            }

            ungrouped.each{|o|
              ungrouped_updated << o.reverse
            }

            s = options_for_select([[]] + ungrouped_updated)
            s << grouped_options_for_select(grouped_updated)

            options = s
          end
          #logger.info(options)
          return options
        end
      end
    end
  end
end

unless QueriesHelper.included_modules.include? JumayBiz::Patches::QueriesHelperPatch
  QueriesHelper.send(:include, JumayBiz::Patches::QueriesHelperPatch)
end
