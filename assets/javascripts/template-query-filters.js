function updateValuesFiltersToSetting(){
  var objResult = [];
  $("#jumay_query_filters").find(".pickListResult option").each(function () {
     objResult.push($(this).data('id'));
  });

  $("#QueryFiltersHidden").val(objResult.join(","));
}

function updateValuesColumnsToSetting(){
  var objResult = [];
  $("#jumay_query_columns").find(".pickListResult option").each(function () {
     objResult.push($(this).data('id'));
  });

  $("#QueryColumnsHidden").val(objResult.join(","));
}

$( document ).ready(function() {
    var pickQueryFilters = $("#jumay_query_filters").pickList({
      data: query_filters_list,
      id : "settings_query_filters",
      buttonsCustomEvent : updateValuesFiltersToSetting
    });

    var pickQueryFilters = $("#jumay_query_columns").pickList({
      data: query_columns_list,
      id : "settings_query_columns",
      buttonsCustomEvent : updateValuesColumnsToSetting
    });
});
