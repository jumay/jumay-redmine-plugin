(function ($) {

   $.fn.pickList = function (options) {

      var opts = $.extend({}, $.fn.pickList.defaults, options);

      this.fill = function () {
         var option = '';

         $.each(opts.data, function (key, val) {
           selected = ''
           if(typeof(val.selected) != 'undefined' && val.selected == true){
             selected = 'selected'
           }
            option += '<option data-id="' + val.id + '" '+ selected +'>' + val.text + '</option>';
         });
         this.find('.pickData').append(option);
      };

      this.fillSelected = function () {
        var p = this.find(".pickData option:selected");
        if(p.length > 0){
          p.clone().appendTo(this.find(".pickListResult"));
          p.remove();
        }
      };

      this.controll = function () {
         var pickThis = this;

         pickThis.find(".pAdd").on('click', function () {
            var p = pickThis.find(".pickData option:selected");
            p.clone().appendTo(pickThis.find(".pickListResult"));
            p.remove();

            if(typeof(opts.buttonsCustomEvent) === "function") opts.buttonsCustomEvent();
         });

         pickThis.find(".pAddAll").on('click', function () {
            var p = pickThis.find(".pickData option");
            p.clone().appendTo(pickThis.find(".pickListResult"));
            p.remove();

            if(typeof(opts.buttonsCustomEvent) === "function") opts.buttonsCustomEvent();
         });

         pickThis.find(".pRemove").on('click', function () {
            var p = pickThis.find(".pickListResult option:selected");
            p.clone().appendTo(pickThis.find(".pickData"));
            p.remove();

            if(typeof(opts.buttonsCustomEvent) === "function") opts.buttonsCustomEvent();
         });

         pickThis.find(".pRemoveAll").on('click', function () {
            var p = pickThis.find(".pickListResult option");
            p.clone().appendTo(pickThis.find(".pickData"));
            p.remove();

            if(typeof(opts.buttonsCustomEvent) === "function") opts.buttonsCustomEvent();
         });
      };

      this.getValues = function () {
         var objResult = [];
         this.find(".pickListResult option").each(function () {
            objResult.push({
               id: $(this).data('id'),
               text: this.text
            });
         });
         return objResult;
      };

      this.init = function () {
        var idSelect = "", nameSelect = "";
        if(opts.id){
          idSelect = "id='"+ opts.id +"'"
        }

        if(opts.name){
          nameSelect = "name='"+ opts.name +"'"
        }

         var pickListHtml =
                 "<div class='row'>" +
                 "  <div class='col-sm-5'>" +
                 "	 <select class='form-control pickListSelect pickData' multiple></select>" +
                 " </div>" +
                 " <div class='col-sm-2 pickListButtons'>" +
                 "	<button type='button' class='pAdd btn btn-primary btn-sm'>" + opts.add + "</button>" +
                 "  <button type='button' class='pAddAll btn btn-primary btn-sm'>" + opts.addAll + "</button>" +
                 "	<button type='button' class='pRemove btn btn-primary btn-sm'>" + opts.remove + "</button>" +
                 "	<button type='button' class='pRemoveAll btn btn-primary btn-sm'>" + opts.removeAll + "</button>" +
                 " </div>" +
                 " <div class='col-sm-5'>" +
                 "    <select "+ idSelect +" "+ nameSelect +" class='form-control pickListSelect pickListResult' multiple></select>" +
                 " </div>" +
                 "</div>";

         this.append(pickListHtml);

         this.fill();
         this.controll();
         this.fillSelected();
      };

      this.init();
      return this;
   };

   $.fn.pickList.defaults = {
      add: 'Add',
      addAll: 'Add All',
      remove: 'Remove',
      removeAll: 'Remove All'
   };


}(jQuery));
