require 'redmine'
require 'jumay_biz'

Redmine::Plugin.register :jumay_biz do
  name 'Jumay Biz plugin'
  author 'Isaac Manuel'
  description 'Agrega funcionalidad especificas para usuarios y clientes de Jumay'
  version '0.0.1'

  default_settings = {
    query_filters: '',
    query_columns: '',
    default_role_assignee_new_issue: '',
    default_tracker_new_issue: 0,
    default_priority_new_issue: 0
  }

  settings(default: default_settings, partial: 'settings/jumay_biz/jumay_biz')

  permission :enable_client_mode, {}
  permission :only_show_own_issues, {}

  menu :admin_menu, :jumay, { controller: 'settings', action: 'plugin', id: 'jumay_biz' }, caption: :label_jumay_biz_menu_admin
  # required redmine version
  requires_redmine version_or_higher: '2.6.0'
end
